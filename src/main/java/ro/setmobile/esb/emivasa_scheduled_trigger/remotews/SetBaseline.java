package ro.setmobile.esb.emivasa_scheduled_trigger.remotews;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/SetBaseline")
public interface SetBaseline {
	@POST
	@Path("/setBaseline")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String getBaseline(String json);
}
