package ro.setmobile.esb.emivasa_scheduled_trigger.remotews;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/GetUsersForCompletion")
public interface GetUsersForCompletion {


	@GET
	@Path("/getUsersForCompletion")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	public String getUsersForCompletion();
}
