	package ro.setmobile.esb.emivasa_scheduled_trigger.aggregationstrategies;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class AggregationStrategyBean implements AggregationStrategy {

	static Logger log = Logger.getLogger(AggregationStrategyBean.class);

	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		if (newExchange == null) {
			return oldExchange;
		}
		log.info("Body from WS " + newExchange.getIn().getBody(String.class));
		List jsonNewMapper;
		List<String> result = new ArrayList<String>();
		ObjectMapper mapper = new ObjectMapper();
		String newBody = newExchange.getIn().getBody(String.class);
		try {
			log.info("ENTER MAPPER");
			jsonNewMapper = mapper.readValue(newBody, List.class);
			if (jsonNewMapper == null) {
				log.info("IS NULL");
			}else
			{
				log.info("COUNT "+jsonNewMapper.size());
				oldExchange.getIn().setHeader("countSize", jsonNewMapper.size());
			}
			for (Object o : jsonNewMapper) {
				String entry = o.toString();
				entry=entry.replaceAll("[{}]", "");
				entry=entry.split("=")[1];
				result.add(entry);
			}
		} catch (Exception e) {
			return newExchange;
		}
		oldExchange.getIn().setBody(result);
		return oldExchange;
	}
}
