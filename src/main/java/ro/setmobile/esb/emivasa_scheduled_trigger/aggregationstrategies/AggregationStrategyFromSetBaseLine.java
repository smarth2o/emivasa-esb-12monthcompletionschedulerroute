package ro.setmobile.esb.emivasa_scheduled_trigger.aggregationstrategies;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.log4j.Logger;
import org.eclipse.jetty.util.log.Log;

public class AggregationStrategyFromSetBaseLine implements
		AggregationStrategy {
	static Logger log = Logger.getLogger(AggregationStrategyBean.class);

	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		log.info("Entering AggregationStrategyFromSetBaseLine");
		String oldStringBody = oldExchange.getIn().getBody(String.class);
		String newStringBody = newExchange.getIn().getBody(String.class);
		log.info("oldStringBody " + oldStringBody);
		log.info("newStringBody " + newStringBody);
		return newExchange;
	}

}
