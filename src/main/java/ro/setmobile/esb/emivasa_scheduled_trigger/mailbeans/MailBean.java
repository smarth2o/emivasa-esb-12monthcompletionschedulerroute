package ro.setmobile.esb.emivasa_scheduled_trigger.mailbeans;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.camel.Body;
import org.apache.log4j.Logger;

public class MailBean {
	static Logger log = Logger.getLogger(MailBean.class);

	public String setBody(@Body List<String> body) {
		String message = "";
		InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("mail_success.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String line;
		try {
			while ((line = br.readLine()) != null) {
				message += line + "\n";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		Calendar calendar = Calendar.getInstance(); // this would default to now
		Date date = calendar.getTime();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String data_stop = df.format(date);

		calendar.add(Calendar.MONTH, -12);
		date = calendar.getTime();
		String data_start = df.format(date);

		for (String s : body) {
			message += s + "\t" + data_start + " -> " + data_stop + "\n";
		}
		return message;
	}
}
