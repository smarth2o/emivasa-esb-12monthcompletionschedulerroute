package ro.setmobile.esb.emivasa_scheduled_trigger.mailbeans;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.camel.Body;
import org.apache.log4j.Logger;

public class MailBeanFail {
	static Logger log = Logger.getLogger(MailBeanFail.class);

	public String setBody(@Body String body) {
		String message = "";
		InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("mail_fail.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String line;
		try {
			while ((line = br.readLine()) != null) {
				message += line + "\n";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (body != null) {
			message += body + "\n";
		}
		return message;
	}

}
