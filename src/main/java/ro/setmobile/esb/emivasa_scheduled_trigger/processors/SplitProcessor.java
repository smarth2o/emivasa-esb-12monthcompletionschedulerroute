package ro.setmobile.esb.emivasa_scheduled_trigger.processors;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.cxf.message.MessageContentsList;
import org.apache.log4j.Logger;
import org.eclipse.jetty.util.log.Log;

public class SplitProcessor implements Processor {
	private static Logger log = Logger.getLogger(SplitProcessor.class);

	@Override
	public void process(Exchange exch) throws Exception {

		Message inMessage = exch.getIn();
		inMessage.setHeader(CxfConstants.OPERATION_NAME, "getData");
		inMessage.setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API,
				Boolean.FALSE);

		inMessage.setHeader("CamelHttpMethod", "GET");
		inMessage.setHeader("Content-Type", "text/plain");

		String httpQuery = "";
		String user_id = exch.getIn().getBody(String.class);
		httpQuery = "user_id=" + user_id;

		Calendar calendar = Calendar.getInstance(); // this would default to now
		Date date = calendar.getTime();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String data_stop = df.format(date);

		calendar.add(Calendar.MONTH, -12);
		date = calendar.getTime();
		String data_start = df.format(date);

		httpQuery += "&date_start=" + data_start;
		httpQuery += "&date_stop=" + data_stop;
		inMessage.setHeader("CamelHttpQuery", httpQuery);

		MessageContentsList req = new MessageContentsList();

		req.add(user_id);
		req.add(data_start);
		req.add(data_stop);
		inMessage.setBody(req);

	}

}
